# Exemple d’utilisation de Framanotes

## Framanotes me sert à préparer mon prochain roman (et faire une tarte)

Pour écrire
son prochain roman, mettant en scène un
[Incube](https://fr.wikipedia.org/wiki/Incube) patron d’un *coffee
shop*, Pouhiou a besoin de rassembler les notes de ses recherches… Il
décide donc de créer un compte sur
[Framanotes.org](https://framanotes.org). Il a bien lu l’avertissement,
et note son mot de passe avec soin, parce que même en tant que salarié
chez Framasoft, il sait qu’il n’y a pas de passe-droit possible&nbsp;: il est
strictement impossible pour l’équipe technique de le lui retrouver s’il
le perd.

![Se créer un compte](images/framanotes-creation-1.png "Se créer un compte")

![Noter son mot de passe](images/framanotes-creation-2.png "Noter son mot de passe")

![Confirmer la création](images/framanotes-creation-3.png "Confirmer la création")

Maintenant, il lui faut installer l’application Turtl [sur son
ordinateur](https://turtlapp.com/download/) (sous Ubuntu) et [son
téléphone](https://play.google.com/store/apps/details?id=com.lyonbros.turtl)
(sous Android/Cyanogen). C’est simple&nbsp;: télécharger, installer, rentrer
son pseudo (avec la majuscule, sinon c’est pas le même) et son mot de
passe, et bien inscrire " https://api.framanotes.org " dans les
"paramètres avancés".

![L’application bureau](images/framanotes-connection-bureau.png "L’application bureau")

![L’application mobile](images/framanotes-connection-mobile.png "L’application mobile")


Bon il est temps de créer sa première note&nbsp;: il fait
un résumé des idées maîtresses pour ce nouveau roman, et utilise la
puissance du [Markdown](https://fr.wikipedia.org/wiki/Markdown), un code
tout léger et facile à utiliser, pour les mettre en page. 

![Écrire en Markdown&hellip;](images/Framanotes-note-texte.png "Écrire en Markdown&hellip;")

![&hellip;et voilà le résultat&nbsp;!](images/Framanote-note-texte-resultat.png "&hellip;et voilà le résultat&nbsp;!")

Il en profite pour ajouter dans ses notes quelques liens et photos qui vont l’inspirer
dans son écriture&nbsp;: des infos sur les incubes, des photos pour le
*coffee shop* où démarre l’intrigue, et le document pdf du dictionnaire
des Furby (puisque son démon sera accompagné de cette peluche possédée).
Malin, il prend soin d’ajouter à chacune l’étiquette «&nbsp;Projet Incube&nbsp;».

![résultat](images/framanote-notes-resultat.png)

![Une note avec un lien](images/Framanote-note-lien.png "Une note avec un lien")

![Une note avec une image](images/Framanote-note-image.png "Une note avec une image")

![Une note avec un fichier](images/framanote-note-fichier.png "Une note avec un fichier")

Dans ses recherches pour les plats
servis au *coffee shop*, il trouve une recette de [tarte crue "avocat-citron vert"](http://dubiodansmonbento.com/tarte-crue-a-lavocat-et-au-citron-vert-vegan-raw/)
qui lui fait vraiment de l’œil. Il décide d’[installer l’extension
Firefox](https://addons.mozilla.org/fr/firefox/addon/turtl/) pour
l’ajouter plus facilement dans ses Framanotes… Vu qu’il utilise
l’application Turtl sur son ordinateur, l’extension Firefox marche comme
un charme&nbsp;! (il sait qu’elle ne fonctionnera pas avec la version web)

![extention firefox](images/framanotes-extention-firefox.png "L’application de bureau et l’extension Firefox s’appairent en un copier-coller&nbsp;!")

Bon, c’est trop alléchant&nbsp;: il lui faut
faire cette tarte. Il crée une note avec tous les ingrédients pour ses
prochaines courses. Le problème, c’est que ça fait tache parmi les notes
sur son roman&nbsp;! Pas de souci&nbsp;: il va donc créer un tableau de notes
"projet incube", puis un deuxième "courses et achats", pour trier encore
plus facilement ses notes&nbsp;! 

![Le tableau du projet Incube](images/framanotes-tableau-Incube.png "Le tableau du projet Incube")

![Et celui des courses&nbsp;!](images/framanotes-tableau-courses.png "Et celui des courses&nbsp;!")

Une fois dans son magasin préféré, il
retrouve le tableau "courses" sur l’application de son ordiphone, et il
retrouve le lien vers la recette ainsi que la liste des ingrédients
nécessaires… Pratique, ce système&nbsp;! 
![tableau mobile](images/framanotes-tableau-mobile-1.png "Par pudeur, Pouhiou n’a pas rajouté «&nbsp;PQ&nbsp;» dans sa liste de courses.")

Le lendemain, il est temps de se remettre au
travail. Framasky, son collègue à Framasoft, vient justement de lui
proposer de partager un tableau de notes des tâches qu’ils ont à faire
pour Dégoogliser Internet&nbsp;! Oh la belle idée&nbsp;!

![partage email](images/framanotes-partage-email.png "L’email du partage de Luc.")

Il lui suffit de cliquer sur
accepter pour que ces notes professionnelles s’ajoutent aux notes perso.
Mais seules celles sur le tableau partagé avec Luc seront visibles par
ce dernier.

![Accepter le partage](images/framanotes-partage-accepter.png "Accepter le partage")

![et on peut collaborer&nbsp;!](images/Framanotes-partage-résultat.png "et on peut collaborer&nbsp;!")

Et voilà, Pouhiou n’a plus qu’à [se mettre à
l’écriture, aux fourneaux et au boulot](http://yakafokon.detected.fr/)&nbsp;!
À aucun moment il ne s’est rendu compte que tous les envois, échanges et
réceptions de notes étaient chiffrés et déchiffrés, car les applications
Turtl le font directement pour lui. 
