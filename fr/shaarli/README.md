# MyFrama

[MyFrama](https://my.framasoft.org) est un service en ligne libre qui permet de conserver, trier, synchroniser et retrouver ses adresses web favorites, dont celles des services Framasoft que vous utilisez.

Il est propulsé par le logiciel libre [Shaarli](https://github.com/shaarli/Shaarli/).

![Exemple d’un compte Framanotes](images/myframa-complet.png)

Tutoriel pour bien débuter&nbsp;: [MyFrama, exemple d’utilisation](exemple-d-utilisation.html)

### Pour aller plus loin&nbsp;:

-   Essayer [MyFrama](https://my.framasoft.org)
-   Application Android
    -   Sur le [store libre
        FDroid](https://f-droid.org/repository/browse/?%20fdid=com.dimtion.shaarlier)
    -   Sur le [Google Play
        Store](https://play.google.com/store/apps/details?id=com.dimtion.shaarlier&hl=fr)
-   [Participer au code](https://github.com/shaarli/Shaarli/) de Shaarli
-   Installer [Shaarli sur vos serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-shaarli/)
-   [Dégooglisons Internet](https://degooglisons-internet.org/)
-   [Soutenir Framasoft](https://soutenir.framasoft.org/)

