# Framaclic, un nouveau service qui compte

Prenons un exemple rapide. Fred aime bien pondre des textes et il les sème un peu partout sur le vaste Ouèbe.

Cela lui pose deux problèmes.

Ses textes sont sur des sites différents avec parfois des adresses web (ou « URL ») longues comme un jour sans pain.
Mais pour ça il a trouvé la parade, c'est frama.link, le raccourcisseur d'URL de Framasoft.
Il a créé une adresse courte pour chacun de ses textes, et quand on lui demande où on peut lire sa prose, il donne cette URL plutôt qu'une adresse de 256 caractères biscornus.
Pour avoir des adresses web encore plus courtes, il pourrait utiliser https://huit.re/.

L’autre souci de Fred, c'est qu’il est affreusement cabotin.
Il écrit pour le plaisir, il publie sous licence libre, il a compris qu'il ne bouclerait pas ses fins de mois grâce à sa plume, mais il ne peut pas s'empêcher de se demander si quelqu'un⋅e lit réellement ce qu'il commet.

Fred est donc tout content quand Framasoft sort Framaclic (bon, il ne fait pas des triples saltos, mais il a un moment de jubilation).

## C'est quoi ?

![Zag, l'adorable mascotte de Dolomon](images/dolomon.png)

Framaclic est un raccourcisseur d'URL qui compte les clics. Voilà. Dit comme ça, on dirait que c'est drôlement simple, non ?

Eh bien, bonne nouvelle : c'est simple !

Bon, soyons justes, Frama.link avait déjà un compteur, rudimentaire.
Il reconnaît l'auteur de l'URL courte via un petit cookie et est capable de lui fournir un comptage des clics.
Seulement, ça ne marche que depuis l'ordinateur et le navigateur sur lesquels l'adresse courte a été créée (à cause du cookie).

Framaclic est un Frama.link dopé aux stéroïdes.

## Comment ça marche ?

Framaclic est basé sur Dolomon, comme DOwnLOad MONitor. Pas besoin d'avoir fait anglais première langue pour piger ça.

Fred se rend sur framaclic.org.
Il crée un compte avec un mot de passe, histoire d'être seul à pouvoir accéder à ses statistiques (des fois qu'elles soient mauvaises).

Il fait une liste des adresses de toutes les ressources vers lesquelles il veut créer un lien : ses textes, son blog, son CV, ses galeries de photos, une BD de Simon qu'il adore partager avec ses collègues…
Si la liste n'est pas exhaustive, ce n'est pas grave, il pourra en ajouter par la suite.

Comme il aime bien que les choses soient correctement rangées (rappel : cet exemple est une fiction), il crée des catégories et des étiquettes pour s'y retrouver.
Surtout qu'il se dit que ce truc-là va drôlement lui rendre service et qu'il va finir par y mettre beaucoup d'adresses.

Ensuite, pour chaque adresse longue il en génère une courte (un "dolo").
Pas besoin de la conserver, Framaclic s'en charge.

![dolo chien](images/ajoutDoloChien-1.png)

Les dolos sont créés au fur et à mesure.

![ajout dolo](images/ajoutDoloVFVVOK.png)

Pour suivre les visites sur une page précise, Fred peut créer un dolo pointant sur une petite image transparente (Dolomon vous en propose une) et insérer l’URL générée, comme on insère une image, dans sa page.

Fred aime surtout créer des dolos qui pointent sur un document, au lieu de la page web.
Par exemple, un dolo pour le pdf de son roman (http://framabook.org/docs/vieuxflic/FredUrbain_VFVV_cczero20151108.pdf au lieu de la page générique https://framabook.org/vieux-flic-et-vieux-voyou/), un autre pour la version e-pub, et encore un pour le code source en Latex.
De cette façon, Fred saura quelle version est la plus téléchargée.

![liste dolos](images/listeDolos-1024x306.png)

Mais il ne saura rien de plus : Framaclic n'enregistre que des statistiques anonymes, pas les adresses IP des visiteureuses.

Par contre, cela fait de beaux graphiques :

![graph framaclic](images/graph-framaclic-1024x307.png)

Et comme vos données vous appartiennent, vous pouvez les télécharger dans un fichier CSV, ce qui vous permettra de les manipuler à votre guise, de faire des camemberts colorés…

Ah, un dernier truc cool à savoir : Luc a fait un plugin Dolomon pour WordPress.
Si vous avez un blog, vous pourrez créer vos dolos directement depuis votre article.

![](https://framaclic.org/h/doc-framaclic)
