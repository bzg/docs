# Framavox

[Framavox](https://framavox.org) est un service en ligne de prise de décision proposé par l'association Framasoft.

Le service repose sur le logiciel libre [Loomio](https://loomio.org).

Voici une documentation pour vous aider à l'utiliser.
Il s’agit de la traduction française de la [documentation officielle](https://loomio.gitbooks.io/manual/content/en/index.html) réalisée par l'équipe [Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/).

## Loomio, c’est quoi&nbsp;?

Loomio est un outil en ligne, simple, facile à utiliser, qui permet des prises de décision collaborative. Loomio vous permet d’engager des discussions en y invitant les personnes concernées. Au moment opportun, Loomio vous permet de prendre des décisions et de transformer vos délibérations en actions dans le monde réel.

Pour mieux cerner ses fonctionalités et les utilisations possible, voire notre [exemple d'utilisation](exemple-d-utilisation.html), ainsi que [les différents types de vote](types-votes.html).

## Comment Loomio fonctionne-t-il&nbsp;?

* **Créez un groupe** pour commencer à collaborer sur Loomio
* **Invitez des personnes** à vous rejoindre dans votre groupe
* **Commencez une discussion** sur le sujet qui vous préoccupe
* **Faites une proposition** afin d’avoir une idée du ressenti de chacun
* **Décidez ensemble** - tout le monde peut donner son accord, s’abstenir, exprimer son désaccord, ou bloquer la proposition - vous aurez un résultat rapide et verrez en un coup d’oeil ce que chacun pense de la proposition.

[Regardez cette courte vidéo](https://framatube.org/media/tutoriel-framavox) qui vous montre comment utiliser Framavox.

---

## Table des matières

* [Avant de commencer](getting_started.html)
* [Réglages de groupe](group_settings.html)
* [Coordonner votre groupe](coordinating_your_group.html)
* [Inviter de nouveaux membres](inviting_new_members.html)
* [Discussion](discussion_threads.html)
* [Commentaires](comments.html)
* [Propositions](proposals.html)
* [Sous-groupes](subgroups.html)
* [Naviguer dans Loomio](reading_loomio.html)
* [Rester à jour](keeping_up_to_date.html)
* [Votre profil](your_user_profile.html)
