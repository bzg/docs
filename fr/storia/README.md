Storia est une application web offrant des fonctionnalités similaires
au service Storify, désormais abandonné.

Storia permet de créer des *stories* : chaque story présente une
histoire à laquelle se mêlent des tweets choisis par l’utilisateur.

Voici comment créer votre première *story* en cinq minutes :

1.  créez votre compte ;
2.  connectez-vous ;
3.  cliquez sur « Créer » ;
4.  ajoutez un paragraphe d’introduction ;
5.  ajouter un tweet ;
6.  publiez !

En dehors de la création de stories, le service permet de sauvegarder
les stories publiées sur storify.


# Créer une story

FIXME: Ajouter une capture d’écran.


# Sauvegarder une story

FIXME: Ajouter une capture d’écran.


# Explorer les stories existantes

FIXME: Ajouter une capture d’écran.

