# Documentation

## Utiliser la Framakey

 * [Installation](installer.html)
 * [Prise en main](utiliser.html)
 * [Ajouter des applications](ajouter-apps.html)


## Liens externes

 * [Framakey](https://framakey.org)
 * [La foire aux questions Framakey](https://contact.framasoft.org/foire-aux-questions/#framakey)
 * [Signaler un bug / proposer une amélioration technique](https://framacolibri.org/c/qualite/framakey)