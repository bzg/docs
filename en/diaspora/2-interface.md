Getting started in diaspora\*
=============================

Part 2 – The interface
----------------------

**Note:** if you are using a handheld device, you will be viewing the diaspora\* interface designed for smaller screens. [Click here](2b-interface-mobile.html) for a guide to the mobile interface.

You’re now looking at an interface that might be unfamiliar to you. (If
you’re still on the edit profile page, click the pod name or Stream at
the left-hand end of the black header bar.) Let’s have a quick look at
the layout and start to get familiar with it.

Because of the decentralized and open-source nature of diaspora\*, it is
possible for pod administrators to customize their pods. Therefore,
depending on which pod you’re on, the interface might look very slightly
different to how it’s described here. However, the essential elements
should all be present, even if they are arranged slightly differently.

At the top of the screen is the header bar, which always remains in
place, even when you scroll down the page. This header bar contains:

![Dropdown](images/interface-1.png)

1.  your pod’s name, which takes you back to the stream page;
2.  links to your **Stream** and **My activity** pages;
3.  a notifications icon (we’ll cover this in [Part
    6](6-conversations.html));
4.  a conversations icon (also covered in [Part
    6](6-conversations.html));
5.  the search bar (more about this in [Part
    4](4-connecting.html));
6.  a personal drop-down menu with:
    -   a link to your profile page (see below);
    -   a link to your contacts list (see [Part
        4](4-connecting.html));
    -   a link to your account settings page (see [Part
        7](7-finishing.html));
    -   a link to a help section;
    -   a log out button.

Beneath the header there are two columns:

![Stream](images/interface-2.jpg)

 

-   In the sidebar you’ll see links to the different content views
    available in diaspora\*: Stream, My activity, @Mentions, My aspects,
    \#Followed tags and Public activity. We’ll explain these next. There
    are also some links to help you invite your friends, welcome new
    users, find help, connect to external services such as Facebook,
    Twitter, Tumblr and Wordpress (if available on your pod), post to
    diaspora\* from the rest of the web (the diaspora\* “bookmarklet”)
    and on some pods, contact information for the pod’s administrator
    and a button to donate to the costs of keeping your pod running.
-   The main column contains the content stream with a “publisher” box
    at the top. This is where the magic happens! We’ll cover using the
    publisher in [Part 5](5-sharing.html).

The different views available in diaspora\* are:

#### The stream

The “stream” is your home page in diaspora\*. It aggregates all the
posts available to you. That is:

1.  posts made by your contacts;
2.  public posts made containing \#tags you follow;
3.  posts that @mention you;
4.  posts from the “Community spotlight,” if your pod has enabled this
    feature (see [Part 7](7-finishing.html)).

Posts in the stream are ordered according to the time of the original
post.

#### My activity

Your “My activity” stream shows any posts which you have either liked or
commented on. This is a useful way of catching up on any interactions
you’ve been having in diaspora\*. Posts in this stream are ordered
according to the latest activity (comment or like) on each post.

#### @Mentions

This displays any posts made by other people @mentioning you. We’ll
discuss @mentions in [Part 4](4-connecting.html).

#### My aspects

“My aspects” displays all posts made by all the people with whom you are
sharing – that is, the people you have placed into your aspects. Don’t
worry if you don’t know what an aspect is – we’re going to cover this in
detail in the next part.

Click on My aspects in the menu to see the list of your aspects. You can
view posts made to all aspects, or just to one or more individual
aspects, by selecting and deselecting them from the aspect list in the
menu.

#### \#Followed tags

This view displays all available posts (public posts and limited posts
made to aspects you have been placed in) containing tags you follow.
We’ll look at tags in more detail in [Part 5](5-sharing.html).

In this view you will see a list of your tags. Clicking on a tag in the
sidebar will take you to the stream for that tag. You can stop following
a tag by clicking the cross by its name in the sidebar.

#### Public activity

The public activity stream displays all public posts available to your
pod, even posts created by people you aren’t sharing with and which
don’t contain any tags you follow. That’s a lot of content!

### Other views

There are other views available in diaspora\*, which you can find by
clicking links.

#### Tag stream

Click on any \#tag in diaspora\* to take you to the “tag stream.” This
shows you a stream containing all the posts your pod knows about which
use that \#tag.

#### Single-post view

Click on the timestamp on any post to view that post and any comments on
it in the “single-post view.” The timestamp is the link by the post
author’s name that reads something like “2 hours ago.” If you want to
send someone a link to a post in diaspora\*, you can grab the post’s
direct URL from here.

#### Profile view

Clicking your name or profile image (your “avatar”) anywhere you see it
takes you to your profile page. This shows you all the posts you have
made in diaspora\*. It also displays your profile photo, screen name,
“about me” tags and your “**diaspora\* ID** (which looks like an email
address, starting with the user name you chose followed by the name of
your pod).

Under your avatar you’ll see any extra profile information you have
added, such as birthday, location, gender, biography and so on.

Click the blue Edit my profile button if you want to change any of your
profile information (see [Part 1](1-sign_up.html)).

You can also view someone else’s profile page by clicking their name or
avatar. How much of a person’s profile information and posts are made
available to you depends on the type of sharing relationship they have
with you.

-   If they are sharing with you, there will be a green ‚úì next to
    their diaspora\* ID. If not, there will be a gray circle.
-   At the top right you’ll find an aspects button: if the person is not
    in one of your aspects, it will read “Add contact;” if they are, it
    will be green and show the name of the aspect you’ve placed them in.
    Click it to add them to an aspect. If you are ignoring that person,
    the button will be red and will read “Stop ignoring.”
-   The following icons will be shown below the aspect selector button:
    a @mention icon if you are sharing with that person, a message
    (envelope) icon if you have mutual sharing, and an ignore icon.

Underneath the person’s name and ID are tabs to display their posts,
their uploaded photos, and any of their contacts which they have made
visible to you (which is explained in the next part, “Aspects”).

#### Color themes

You can customise how diaspora\* looks by choosing one of our color
themes from the Settings menu (see [Part
7](7-finishing.html)). The current themes are:

-   **Original dark**: this is the default diaspora\* theme
-   **Original white background**: the default theme, but with a white
    background. This looks more like the “old-school” diaspora\*
    interface
-   **Dark green**
-   **Magenta**
-   **Egyptian blue**

Your pod might have created some custom themes. If so, they’ll show up
in the list along with our default themes.

 

That’s all you need to know about the interface for now. The first thing
you will want to do is finding some people to start sharing with.
However, before we get to that, let’s focus on something that features
prominently throughout diaspora\*: **aspects**.

 

[Part 1 – Signing
up](1-sign_up.html) | [Part 3 –
Aspects](3-aspects.html)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
